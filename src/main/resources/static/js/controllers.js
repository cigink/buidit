var app = angular.module('project');

app.controller('PHRController', function($scope, $http, $route) {
	$scope.phrs = [];
	$http.get('/rest/phr').success(function(data, status, headers, config) {
		console.log(data);
		$scope.phrs = data;
	});
	
	$scope.follow = function(link) {
		$http.post(link.href).success(function(data, status, headers, config) {
			console.log(data);
			$route.reload();
		});
	};
});

app.controller('CreatePHRController', function($scope, $http, $location) {
	$scope.plants = [];
	$scope.name = '';
	$scope.startDate = new Date();
	$scope.endDate = new Date();
	
	$scope.catalogShown = false;
	
	$scope.execQuery = function () {
		$http.get('/rest/plants', {params: {name: $scope.name}}).success(function(data, status, headers, config) {
			$scope.plants = data;
			$scope.catalogShown = true;
		});
	};
	$scope.setPlant = function (selectedPlant) {
		phr = { plant: selectedPlant, startDate: $scope.startDate, endDate: $scope.endDate};
		
		$http.post('/rest/phr', phr).success(function(data, status, headers, config) {
			console.log(status);
			$location.path('/phr');
		});
	};
});

app.controller('PlantsController', function($scope, $http) {
	$scope.plants = [];
	$http.get('/rest/plants').success(function(data, status, headers, config) {
		console.log(JSON.stringify(data));
		$scope.plants = data;
	});
	
});