var app = angular.module('project', ['ngRoute']);

app.config(function($routeProvider) {
	$routeProvider
	    .when('/plant', {
	      controller:'PlantsController',
	      templateUrl:'views/plants/list.html'
	    })
	    .when('/phr', {
	      controller: 'PHRController',
	      templateUrl: 'views/phr/list.html'
	    })
	    .when('/phr/create', {
	      controller: 'CreatePHRController',
	      templateUrl: 'views/phr/create.html'
	    })
	    .otherwise({
	      redirectTo:'/phr'
	    });
	})