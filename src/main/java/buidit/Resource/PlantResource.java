package buidit.Resource;

import javax.xml.bind.annotation.XmlRootElement;

import lombok.Getter;
import lombok.Setter;
import buildit.util.ResourceSupport;

@Getter
@Setter
@XmlRootElement(name = "plant")
public class PlantResource extends ResourceSupport {

	Long idres;
	String Name;
	String Description;
	Float Price;

}
