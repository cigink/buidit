package buidit.Resource;

import java.util.Date;

import javax.xml.bind.annotation.XmlRootElement;

import lombok.Getter;
import lombok.Setter;
import buildit.models.SiteEngineer;
import buildit.models.WorksEngineer;
import buildit.util.ResourceSupport;

@Getter
@Setter
@XmlRootElement(name = "plant_hire_request")
public class PlantHireRequestResource extends ResourceSupport {

	Long idRes;
	PlantResource plantResource;
	SiteEngineer siteEngineer;
	WorksEngineer worksEngineer;
	Float price;
	Date startDate;
	Date endDate;

}
