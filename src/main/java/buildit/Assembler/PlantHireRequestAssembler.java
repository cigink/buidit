package buildit.Assembler;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

import java.util.ArrayList;
import java.util.List;

import org.springframework.hateoas.mvc.ResourceAssemblerSupport;

import buidit.Resource.PlantHireRequestResource;
import buildit.controllers.PHRController;
import buildit.models.PlantHireRequest;
import buildit.util.ExtendedLink;

public class PlantHireRequestAssembler extends
		ResourceAssemblerSupport<PlantHireRequest, PlantHireRequestResource> {

	PlantResourceAssembler plantAssembler;

	public PlantHireRequestAssembler() {
		super(PHRController.class, PlantHireRequestResource.class);
	}

	@Override
	public PlantHireRequestResource toResource(PlantHireRequest phr) {
		PlantHireRequestResource phrResource = createResourceWithId(
				phr.getId(), phr);
		phrResource.setPrice(phr.getPrice());
		phrResource.setEndDate(phr.getEndDate());
		// phrResource.setPlantResource(plantAssembler.toResource(phr.getPlant()));
		phrResource.setStartDate(phr.getStartDate());

		switch (phr.getStatus()) {
		case PENDING_CONFIRMATION:
			phrResource.add(new ExtendedLink(linkTo(
					methodOn(PHRController.class).rejectPhr(phr.getId()))
					.toString(), "rejectPHR", "DELETE"));
			phrResource.add(new ExtendedLink(linkTo(
					methodOn(PHRController.class).acceptPhr(phr.getId()))
					.toString(), "acceptPHR", "POST"));
			break;
		// case ACCEPTED:
		// phrResource.add(new ExtendedLink(linkTo(
		// methodOn(PHRController.class).acceptPhr(phr.getId()))
		// .toString(), "acceptPOUpdate", "POST"));
		// break;
		case REJECTED:
			phrResource.add(new ExtendedLink(linkTo(
					methodOn(PHRController.class).rejectPhr(phr.getId()))
					.toString(), "rejectPHR", "PUT"));
			break;
		// case CLOSED:
		// break;
		case ACCEPTED:
			phrResource.add(new ExtendedLink(linkTo(
					methodOn(PHRController.class).closePHR(phr.getId()))
					.toString(), "closePHR", "PUT"));
		default:
			break;
		}

		return phrResource;
	}

	public List<PlantHireRequestResource> toResource(List<PlantHireRequest> pos) {
		List<PlantHireRequestResource> poress = new ArrayList<>();

		for (PlantHireRequest po : pos) {
			poress.add(toResource(po));
		}
		return poress;
	}

}
