package buildit.Assembler;

import java.util.ArrayList;
import java.util.List;

import buidit.Resource.PlantResource;
import buildit.models.Plant;

public class PlantResourceAssembler {
	public PlantResource toResource(Plant plant) {
		PlantResource res = new PlantResource();

		res.setName(plant.getName());
		res.setDescription(plant.getDesc());
		res.setPrice(plant.getPrice());
		return res;
	}

	public List<PlantResource> toResource(List<Plant> plants) {
		List<PlantResource> pRess = new ArrayList<>();

		for (Plant plant : plants) {
			pRess.add(toResource(plant));
		}
		return pRess;
	}
}
