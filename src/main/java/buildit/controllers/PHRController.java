package buildit.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import buidit.Resource.PlantHireRequestResource;
import buildit.Assembler.PlantHireRequestAssembler;
import buildit.models.PlantHireRequest;
import buildit.models.Status;
import buildit.repositories.PHRRepository;
import buildit2.exception.PHRNotFoundException;

@RestController
@RequestMapping("rest/phr")
public class PHRController {

	@Autowired
	PHRRepository phrRepo;

	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public ResponseEntity<PlantHireRequestResource> getPHR(
			@PathVariable("id") Long id) throws PHRNotFoundException {
		PlantHireRequest phr = phrRepo.findOne(id);
		if (phr == null) {
			throw new PHRNotFoundException(null);
		}
		PlantHireRequestAssembler assembler = new PlantHireRequestAssembler();
		ResponseEntity<PlantHireRequestResource> resp = new ResponseEntity<PlantHireRequestResource>(
				assembler.toResource(phr), HttpStatus.OK);
		return resp;
	}

	@RequestMapping(method = RequestMethod.GET)
	public List<PlantHireRequestResource> getAll() {
		List<PlantHireRequest> pos = phrRepo.findAll();
		PlantHireRequestAssembler assemb = new PlantHireRequestAssembler();
		List<PlantHireRequestResource> resources = assemb.toResource(pos);
		return resources;
	}

	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public ResponseEntity<PlantHireRequestResource> createPO(
			@RequestBody PlantHireRequestResource res)
			throws PHRNotFoundException {

		ResponseEntity<PlantHireRequestResource> resp;
		if (res == null) {
			throw new PHRNotFoundException(null);
		}
		PlantHireRequest phr = new PlantHireRequest();
		phr.setStartDate(res.getStartDate());
		phr.setEndDate(res.getEndDate());
		if (res.getPlantResource() != null) {

			// phr.setId(res.getIdRes());
			phr.setSiteEngineer(res.getSiteEngineer());
			phr.setWorksEngineer(res.getWorksEngineer());
			/*
			 * Plant plant = new Plant(); //
			 * plant.setId(res.getPlantResource().getIdres());
			 * plant.setName(res.getPlantResource().getName());
			 * plant.setDesc(res.getPlantResource().getDescription());
			 * plant.setPrice(res.getPlantResource().getPrice()); Plant
			 * savedPlant = plantRepo.saveAndFlush(plant);
			 */

			phr.setPlantRef(res.getPlantResource().getId().toString());

			phr.setPrice(res.getPrice());
		}
		// if (plantRepo.isPlantAvailable(phr.getPlant().getName(),
		// phr.getStartDate(), phr.getEndDate()).isEmpty()) {
		// // Throw exception if plant NOT available
		// throw new PHRNotFoundException(phr.getPlant().getId());
		// }
		// Compute the total cost of the hiring
		// po.calcCost(po.getPlant());
		phr.setStatus(Status.PENDING_CONFIRMATION);
		PlantHireRequest new_phr = phrRepo.saveAndFlush(phr);
		// Return CREATED status after PO is successfully created
		PlantHireRequestAssembler assemb = new PlantHireRequestAssembler();
		PlantHireRequestResource newRes = assemb.toResource(new_phr);
		resp = new ResponseEntity<PlantHireRequestResource>(newRes,
				HttpStatus.CREATED);
		return resp;
	}

	@RequestMapping(value = "/{id}/accept", method = RequestMethod.DELETE)
	public ResponseEntity<PlantHireRequestResource> rejectPhr(
			@PathVariable("id") Long id) {
		PlantHireRequest phr = phrRepo.findOne(id);
		phr.setStatus(Status.REJECTED);
		// phrRepo.delete(id);
		phrRepo.saveAndFlush(phr);
		PlantHireRequestAssembler assembler = new PlantHireRequestAssembler();
		PlantHireRequestResource newRes = assembler.toResource(phr);
		ResponseEntity<PlantHireRequestResource> resp = new ResponseEntity<PlantHireRequestResource>(
				newRes, HttpStatus.OK);
		return resp;
	}

	@RequestMapping(value = "/{id}/accept", method = RequestMethod.POST)
	public ResponseEntity<PlantHireRequestResource> acceptPhr(
			@PathVariable("id") Long id) {
		PlantHireRequest phr = phrRepo.findOne(id);
		phr.setStatus(Status.ACCEPTED);
		// phrRepo.delete(id);
		phrRepo.saveAndFlush(phr);
		PlantHireRequestAssembler assemb = new PlantHireRequestAssembler();
		PlantHireRequestResource newRes = assemb.toResource(phr);
		ResponseEntity<PlantHireRequestResource> resp = new ResponseEntity<PlantHireRequestResource>(
				newRes, HttpStatus.OK);

		return resp;
	}

	@RequestMapping(value = "/{id}/accept", method = RequestMethod.PUT)
	public ResponseEntity<Void> closePHR(@PathVariable("id") Long id) {
		PlantHireRequest phr = phrRepo.findOne(id);
		phr.setStatus(Status.CLOSED);
		phrRepo.delete(id);
		phrRepo.saveAndFlush(phr);
		ResponseEntity<Void> resp = new ResponseEntity<Void>(HttpStatus.OK);
		return resp;
	}

	@RequestMapping(value = "/{phr.id}/", method = RequestMethod.POST)
	public ResponseEntity<PlantHireRequestResource> updatePhr(
			@RequestBody PlantHireRequestResource res,
			@PathVariable("id") Long id) throws PHRNotFoundException {

		ResponseEntity<PlantHireRequestResource> response;

		if (res == null) {
			response = new ResponseEntity<PlantHireRequestResource>(
					HttpStatus.NOT_FOUND);
			return response;
		}

		if (phrRepo.findOne(res.getIdRes()) == null) {
			throw new PHRNotFoundException(res.getIdRes());
		}

		PlantHireRequest phr = new PlantHireRequest();
		phr.setStartDate(res.getStartDate());
		phr.setEndDate(res.getEndDate());
		if (res.getPlantResource() != null) {

			phr.setId(res.getIdRes());
			phr.setSiteEngineer(res.getSiteEngineer());
			phr.setWorksEngineer(res.getWorksEngineer());
			// phr.setPlant(res.getPlant());
			phr.setPrice(res.getPrice());
		}

		// po.calcCost(po.getPlant());
		phr.setStatus(Status.PENDING_CONFIRMATION);
		PlantHireRequest new_phr = phrRepo.saveAndFlush(phr);
		// Return CREATED status after PO is successfully created
		PlantHireRequestAssembler assemb = new PlantHireRequestAssembler();
		PlantHireRequestResource newRes = assemb.toResource(new_phr);
		response = new ResponseEntity<PlantHireRequestResource>(newRes,
				HttpStatus.CREATED);
		return response;
	}

	@RequestMapping(value = "/{phr.id}/updates/{up.id}/accept", method = RequestMethod.DELETE)
	public ResponseEntity<PlantHireRequestResource> rejectPhrUpdate(
			@RequestBody Long id) {
		return null;
	}

	@RequestMapping(value = "/{po.id}/updates/{up.id}/accept", method = RequestMethod.POST)
	public ResponseEntity<PlantHireRequestResource> acceptPhrUpdate(
			@RequestBody Long id) {
		PlantHireRequest phr = new PlantHireRequest();
		phr.setStatus(Status.PENDING_CONFIRMATION);
		return null;
	}

	@ExceptionHandler(PHRNotFoundException.class)
	@ResponseStatus(HttpStatus.NOT_FOUND)
	public void handPONotFoundException(PHRNotFoundException ex) {
	}

}