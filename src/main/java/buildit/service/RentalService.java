//package buildit.service;
//
//import java.text.SimpleDateFormat;
//import java.util.Arrays;
//import java.util.Date;
//import java.util.List;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Service;
//import org.springframework.web.client.RestTemplate;
//
//import buidit.Resource.PlantResource;
//
//@Service
//public class RentalService {
//	@Autowired
//	RestTemplate restTemp;
//
//	public List<PlantResource> findAvailablePlants(String plantName,
//			Date startDate, Date endDate) {
//		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
//
//		PlantResource[] plants = restTemp
//				.getForObject(
//						"http://localost:8080/rest/plants?name={name}&startDate={start}&endDate={end}",
//						PlantResource[].class, plantName,
//						formatter.format(startDate), formatter.format(endDate));
//
//		return Arrays.asList(plants);
//	}
//}
